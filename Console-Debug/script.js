console.log('Hello World'); // Show text "Hello World" at console
console.log(4+6); // Show results 4+6
console.error('Thiis is error'); // Show alert error at console
console.warn('This is warning'); // Show alert warning at console
console.log("%cHello World", "color:green"); //Show text "Hello World" with color green
console.log("%cHello World", "color:blue;background:black"); //Show text "Hello World" with color blue and background black